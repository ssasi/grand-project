from flask import Flask,jsonify,render_template, request, session,redirect,flash,url_for
from flask_mysqldb import MySQL
import MySQLdb.cursors 
app = Flask(__name__)
app.secret_key = 'sasi'
 
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Innominds'
app.config['MYSQL_DB'] = 'aadhar'
mysql = MySQL(app)
 

@app.route('/login/<int:aadhar_number>/<int:OTP>')
def login(aadhar_number,OTP):
	
		cursor = mysql.connection.cursor()
		cursor.execute(f'SELECT * FROM login WHERE aadhar_number ="{aadhar_number}"  and OTP = "{OTP}"')
		account = cursor.fetchone()
		if account:
				session['loggedin'] = True
				return render_template('home.html')
		
		return render_template('login.html')

@app.route('/verify/<int:aadhar_number>')
def verify(aadhar_number):
	
		aadhar_number = request.args['aadhar number']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM verify WHERE aadhar_number ="{aadhar_number}"')
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO verify VALUES (%d)', (aadhar_number))
			mysql.connection.commit()
			data = {
	"msg" : 'You have successfully registered !',
	"aadhar_number":123456,
	"status": "active"
			}
			return jsonify(data)


@app.route('/download/<int:aadhar_number>/<int:OTP>')
def download_e_aadhar(aadhar_number,OTP):
	aadhar_number = request.args['aadhar number']
	OTP = request.args['OTP']
	cursor = mysql.connection.cursor()
	cursor.execute('SELECT * FROM download WHERE aadhar_number = "{aadhar_number}" ,OTP="{OTP}"')
	account = cursor.fetchone()
	if account:
		data = {
		"msg" : 'Account already exists !'
		}
		return jsonify(data)
	else:
		cursor.execute('INSERT INTO verify VALUES (%d,%d)', (aadhar_number,OTP))
		mysql.connection.commit()
		data = {
			"msg" : 'You have successfully registered !',
			"aadhar_number":123456,
			"OTP":9865,
			"status": "active"
		}
	return jsonify(data)

	
@app.route('/retrieve/<int:aadhar_number>/<string:mobilenumber>/<string:email_address>/<int:OTP>')
def retrieve_aadhar(aadhar_number,mobile_number,email_address,OTP):
	aadhar_number = request.args['aadhar number']
	mobile_number=request.args['mobile number']
	email_address=request.args['email address']
	OTP = request.args['OTP']
	cursor = mysql.connection.cursor()
	cursor.execute('SELECT * FROM retrieve WHERE aadhar_number = % d AND mobile_number=% s or email_address=%  and OTP= %s', (aadhar_number, mobile_number,email_address,OTP ))
	account = cursor.fetchone()  
	if account:
		data = {
		"msg" : 'Account already exists !'
		}
		return jsonify(data)
	else:
		cursor.execute('INSERT INTO verify VALUES (%d)', (aadhar_number))
		mysql.connection.commit()
		data = {
	"msg" : 'You have successfully registered !',
	"aadhar_number":123456,
	"mobile_number":"9989723749",
	"email_address":"sasi@gmail.com",
	"OTP":9865,
	"status": "active"
	}
	return jsonify(data)

@app.route('/BankLinkStatus/<int:aadhar_number>/<int:virtual_id>/<int:OTP>')
def bank_linking_status(aadhar_number,virtual_id,OTP):
		aadhar_number = request.args['aadhar number']
		virtual_id=request.args['virtual_id']
		OTP = request.args['OTP']
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM bank_linking_status WHERE aadhar_number = % d or virtual_id = % d AND OTP= %s', (aadhar_number or virtual_id,OTP ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO bank_linking_status VALUES (%d)', (aadhar_number))
			mysql.connection.commit()
			data = {
				"msg" : 'You have successfully registered !',
				"aadhar_number":"123456",
				"virtual_id":"9989723749",
				"OTP":"9865",
				"status": "active"
			}
			return jsonify(data)
@app.route('/unlock/<int:virtual_id>/<string:fullname>/<int:pincode>/<int:OTP>')
def lock_unlock_biometrics(virtual_id,full_name,pincode,OTP):
	
		virtual_id = request.args['virtual id']
		full_name=request.args['full name']
		pincode=request.args['pincode']
		OTP = request.args['OTP']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM unlock1 WHERE virtual_id = % d,full_name= % s,pincode=% d,OTP=%d', (virtual_id,full_name,pincode, OTP ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO unlock1 VALUES (%d)', (virtual_id))
			mysql.connection.commit()
			data = {
				"msg" : 'You have successfully registered !',
		
				"virtual_id":9989723749,
				"fullname":"sasisathi",
				"pincode":974937,
				"OTP":9865,
				"status": "active"
			}
			return jsonify(data)
@app.route('/update/<string:enrollment_id>')
def update(enrollment_id):
		enrollment_id = request.args['enrollment_id']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM update1 WHERE enrollment_id = % s', (enrollment_id ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO update1 VALUES (%s)', (enrollment_id))
			mysql.connection.commit()
			data = {
				"msg" : 'verification is completed !',

				"enrollment_id":"24598475484",

				"status": "active"
			}
		return jsonify(data)



@app.route('/emailverify/<string:aadhar_number>/<string:mobilenumber>/<int:OTP>')
def emailverify(aadhar_number,mobile_number,OTP):
		aadhar_number = request.args['aadhar_number']
		mobile_number = request.args['mobile_number']
		OTP = request.args['OTP']
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM emailverify WHERE aadhar_number = % s,mobile_number=%s,OTP=%s', (aadhar_number,mobile_number,OTP ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO update1 VALUES (%s)', (aadhar_number,mobile_number,OTP))
			mysql.connection.commit()
			data = {
				"msg" : 'verification is completed !',

				"aadhar_number":"9989723749",
				"mobile_number":"23466890",
				"OTP":"9865",

				"status": "active"
			}
		return jsonify(data)

@app.route('/authentication/<string:aadhar_number>/<int:OTP>')
def authentication(aadhar_number,OTP):
	
		aadhar_number = request.args['aadhar_number']
		OTP = request.args['OTP']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM authentication WHERE aadhar_number = % s,OTP=%s', (aadhar_number,OTP ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO update1 VALUES (%s)', (aadhar_number,OTP))
			mysql.connection.commit()
			data = {
				"msg" : 'verification is completed !',

				"aadhar_number":"9989723749",
				"OTP":"9865",

				"status": "active"
			}
		return jsonify(data)


@app.route('/complaint_status/<string:complaint_id>')
def complaint_status(complaint_id):
		complaint_id = request.args['complaint_id']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM complaint_status WHERE complaint_id = % s', (complaint_id ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO complaint_status VALUES (%s)', (complaint_id))
			mysql.connection.commit()
			data = {
				"msg" : 'verification is completed !',

				"complaint_id":"2454",

				"status": "active"
			}
		return jsonify(data)

@app.route('/generate_vid/<string:aadhar_number>/<int:OTP>')
def generate_vid(aadhar_number,OTP):
		aadhar_number = request.args['aadhar_number']
		OTP = request.args['OTP']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM generate_vid WHERE aadhar_number = % s,OTP=%s', (aadhar_number,OTP ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO update1 VALUES (%s,%s)', (aadhar_number,OTP))
			mysql.connection.commit()
			data = {
				"msg" : 'vid is generated !',

				"aadhar_number":"9989723749",
				"OTP":"9865",

				"status": "active"
			}
		return jsonify(data)

@app.route('/order_pvc/<string:aadhar_number>/<int:OTP>')
def order_pvc(aadhar_number,OTP):
	
		aadhar_number = request.args['aadhar_number']
		OTP = request.args['OTP']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM order_pvc WHERE aadhar_number = % s,OTP=%s', (aadhar_number,OTP ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO update1 VALUES (%s,%s)', (aadhar_number,OTP))
			mysql.connection.commit()
			data = {
				"msg" : 'vid is generated !',

				"aadhar_number":"9989723749",
				"OTP":"9865",

				"status": "active"
			}
		return jsonify(data)

@app.route('/order_status/<string:SRN>')
def order_status(SRN):

		SRN= request.args['SRN']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM order_status WHERE SRN = % s', (SRN ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO complaint_status VALUES (%s)', (SRN))
			mysql.connection.commit()
			data = {
				"msg" : 'verification is completed !',

				"SRN":"2454983927483",

				"status": "active"
			}
		return jsonify(data)

@app.route('/update_history/<string:aadhar_number>/<int:OTP>')
def update_history(aadhar_number,OTP):
	
		aadhar_number = request.args['aadhar_number']
		OTP = request.args['OTP']
		cursor = mysql.connection.cursor()
		cursor.execute('SELECT * FROM update_history WHERE aadhar_number = % s,OTP=%s', (aadhar_number,OTP ))
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !'
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO update_history VALUES (%s,%s)', (aadhar_number,OTP))
			mysql.connection.commit()
			data = {
				"msg" : ' updated history is as follows !',

				"aadhar_number":"9989723749",
				"OTP":"9865",

				"status": "active"
			}
		return jsonify(data)
if __name__ =='__main__':  
		app.secret_key='sasi'
		app.run(debug=True,port=5001)


    