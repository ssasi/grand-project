from flask import Flask, render_template, request, session
from flask_mysqldb import MySQL
import MySQLdb.cursors 
app = Flask(__name__)
app.secret_key = 'sasi'
 
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Innominds'
app.config['MYSQL_DB'] = 'aadhar'
 
mysql = MySQL(app)
 
@app.route('/')
@app.route('/login', methods =['GET', 'POST'])
def login():
    msg = ''
    if request.method == 'POST' and 'aadhar number' in request.form and 'captcha' in request.form:
        aadhar_number = request.form['aadhar number']
        captcha = request.form['captcha']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM login WHERE aadhar_number = % s AND captcha = % s', (aadhar_number, captcha ))
        account = cursor.fetchone()
        if account:
            session['loggedin'] = True
            session['aadhar number'] = account['aadhar number']
            msg = 'Logged in successfully !'
            return render_template('login.html', msg = msg)
        else:
            msg = 'Incorrect aadhar number / captcha !'
    return render_template('login.html', msg = msg)
@app.route('/verify aadhar', methods =['GET', 'POST'])
def verify():
    msg=''
    if request.method == 'POST' and 'aadhar number' in request.form and 'captcha' in request.form:
        aadhar_number = request.form['aadhar number']
        captcha = request.form['captcha']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM verify WHERE aadhar_number = % s AND captcha = % s', (aadhar_number, captcha ))
        account = cursor.fetchone()
        if account:
            msg = 'Aadhar verification completed !\n ageband    20-30years\n gender     female\n state      AP\n mobile     xxxxxxx908'
        elif not aadhar_number or not captcha:
            msg = 'Enter the valid details !'
        else:
            cursor.execute('INSERT INTO verify VALUES (NULL, % s, % s)', (captcha, aadhar_number, ))
            mysql.connection.commit()
            msg = 'aadhar verification completed !'
    elif request.method == 'POST':
        msg = 'Please enter the valid details !'
    return render_template('verify.html', msg = msg)
@app.route('/download aadhar', methods =['GET', 'POST'])
def download_e_aadhar():
    msg=''
    if request.method == 'POST' and 'aadhar number' in request.form and 'captcha' in request.form:
        aadhar_number = request.form['aadhar number']
        captcha = request.form['captcha']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM download WHERE aadhar_number = % s AND captcha = % s', (aadhar_number, captcha ))
        account = cursor.fetchone()
        if account:
            msg = 'Account  exists\n "state":"28", "name":"Deva Harshitha"\n "gender":"FEMALE"\n "dob":"2000-10-20"\n"refId":null,"ddistrictName":"Srikakulam"\nstateName:"Andhra Pradesh"\n"subDistrict":null\n"subDistrictLocalName":null\n"subDistrictName":null\n"updatedEIds":[]\n"updatedRefIds":[]\n"biometricFlag":false\n"dobStatus":"V"\n"enrolmentDate":"2019-10-10T10:09:52"\n"updatedEIdsCount":0\n"updatedRefIdsCount":0 !'
        elif not aadhar_number or not captcha:
            msg = 'Enter the valid details !'
        else:
            cursor.execute('INSERT INTO login VALUES (NULL, % s, % s)', (captcha, aadhar_number, ))
            mysql.connection.commit()
            msg = 'download aadhar  !'
    elif request.method == 'POST':
        msg = 'Please enter the valid details !'
    return render_template('download.html', msg = msg)
@app.route('/retrieve aadhar', methods =['GET', 'POST'])
def retrieve_aadhar():
    msg=''
    if request.method=='POST' and 'name' in request.form and 'mobile number' in request.form or 'email address' in request.form and 'captcha' in request.form:
        aadhar_number = request.form['aadhar number']
        mobile_number=request.form['mobile number']
        email_address=request.form['email address']
        captcha = request.form['captcha']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM retrieve WHERE aadhar_number = % s AND mobile_number=% s or email_address=% s captcha = % s', (aadhar_number, mobile_number,email_address,captcha ))
        account = cursor.fetchone()  
        if account:
            msg='Account exists\n otpTxnId: "mAadhaar:c5c47a4b-2062-4af0-9618-1b5a3ef6506c"\n requestDate: null\n status: "Success"\n uidNumber: null\n status: "Success"' 
        elif not(aadhar_number, mobile_number or email_address, captcha):
            msg = 'Enter the valid details !'
        else:
            cursor.execute('INSERT INTO login VALUES (NULL, % s, % s or % s,%s)', (captcha, mobile_number or email_address,aadhar_number))
            mysql.connection.commit()
            msg = 'retrieve aadhar  !'
    elif request.method == 'POST':
        msg = 'Please enter the valid details !'
    return render_template('retrieve.html', msg = msg) 
@app.route('/bank_linking_status', methods =['GET', 'POST'])
def bank_linking_status():
    msg=''
    if request.method == 'POST' and 'aadhar number' in request.form or 'virtual id' in request.form and  'captcha' in request.form:
        aadhar_number = request.form['aadhar number']
        virtual_id=request.form['virtual_id']
        captcha = request.form['captcha']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM bank_linking_status WHERE aadhar_number = % s or virtual_id = % s AND captcha = % s', (aadhar_number or virtual_id, captcha ))
        account = cursor.fetchone()
        if account:
          msg='vidNumber: "9124 5961 2599 5562"\n status: "Success"\n statusCode: 200\n statusMessage: "OK"'
          return render_template('link.html', msg = msg)
        else:
            cursor.execute('INSERT INTO login VALUES (NULL, % s or %s, %s)', (captcha,  aadhar_number or virtual_id ))
            mysql.connection.commit()
            msg = 'bank account linked successfully !'
    elif request.method == 'POST':
        msg = 'Please enter the valid details !'
    return render_template('link.html', msg = msg) 
@app.route('/lock_unlock_biometrics', methods =['GET', 'POST'])
def lock_unlock_biometrics():
    msg=''
    if request.method == 'POST' and 'virtual id' in request.form and 'full name' in request.form and 'pincode' in request.form and 'captcha' in request.form:
        virtual_id = request.form['virtual id']
        full_name=request.form['full name']
        pincode=request.form['pincode']
        captcha = request.form['captcha']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM unlock1 WHERE virtual_id = % s,full_name= % s,pincode=% s, captcha = % s', (virtual_id,full_name,pincode, captcha ))
        account = cursor.fetchone()
        if account:
          msg='aadhaarNumber: "212553464460"\n isrequestProcessed: true \n message: "Your biometrics have been locked successfully"\n requestDate: "2022-08-19"\n status: "Success"'
          return render_template('unlock.html', msg = msg)
        elif account:
            msg='isrequestProcessed: true\n message: "Your biometrics have been unlocked successfully"\n requestDate: "2022-08-19"\n status: "Success"'
        else:
            cursor.execute('INSERT INTO unlock1 VALUES (NULL, % s, % s, % s,%s)', (captcha, full_name , pincode,virtual_id))
            mysql.connection.commit()
            msg = 'lock or unlock biometrics !'
    elif request.method == 'POST':
        msg = 'Please enter the valid details !'
    return render_template('unlock.html', msg = msg)
@app.route('/check_update1', methods =['GET', 'POST'])
def check_update1():
    msg=''
    if request.method == 'POST' and 'Enrollment ID,SRN or URN' in request.form and  'captcha' in request.form:
        enrollment_id = request.form['enrollment id']
        captcha = request.form['captcha']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM check_update1 WHERE aadhar_number = % s AND captcha = % s', (enrollment_id, captcha ))
        account = cursor.fetchone()
        if account:
          msg='creationDateTime: "Fri Aug 19 11:06:10 UTC 2022"\n lastUpdatedDateTime: "Fri Aug 19 11:06:10 UTC 2022"\n payableAmt: "25.00"\n requestDescription: "successfully updated"\n uidNumber: "212553464460"\nurn: "S2254472091000"\n status: "Success"'
          return render_template('update.html', msg = msg)
        else:
            cursor.execute('INSERT INTO check_update1 VALUES (NULL, % s, %s)', (captcha,  enrollment_id ))
            mysql.connection.commit()
            msg = 'updated successfully !'
    elif request.method == 'POST':
        msg = 'Please enter the valid details !'
    return render_template('update.html', msg = msg)
if __name__ =='__main__':  
    app.run()